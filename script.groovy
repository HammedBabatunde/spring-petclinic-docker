def versionIncrement() {
    echo 'incrementing app version...'
    sh 'mvn build-helper:parse-version versions:set \
        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
        versions:commit'
    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
    def version = matcher[0][1]
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}

def testJar() {
    echo 'compiling the pet application'
    sh 'mvn test'
}

def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the pet application image"
    withCredentials([usernamePassword(credentialsId: 'azure-acr', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t basicacr004.azurecr.io/pet-app:${IMAGE_NAME} ."
        sh "echo $PASS | docker login -u $USER --password-stdin basicacr004.azurecr.io"
        sh "docker push basicacr004.azurecr.io/pet-app:${IMAGE_NAME}"
    }
} 

def deployApp() {
    echo 'deploying the application...'
}

def commitVersionUpdate() {
    withCredentials([usernamePassword(credentialsId: 'gitlab-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        // git config here for the first time run
        sh 'git config --global user.email "olawale670@gmail.com"'
        sh 'git config --global user.name "Jenkins"'

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/HammedBabatunde/spring-petclinic-docker.git"
        sh 'git add .'
        sh 'git commit -m "ci: skip_ci"'
        sh 'git push origin HEAD:main'
    }
}

return this
